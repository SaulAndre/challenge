from django.shortcuts import render, redirect
from lab5.models import Aktivitas
from lab5.forms import FormAktivitas

def index(request):
    return render(request, 'lab5/home.html')

def keahlian(request):
    return render(request, 'lab5/do.html')

def kontak(request):
    return render(request, 'lab5/kontak.html')

def kegiatan(request):
    if request.method == 'POST':
        form = FormAktivitas(request.POST)
        if form.is_valid():
            form.save()

            form = FormAktivitas()
            context = {'form':form}
            return render(request, 'lab5/kegiatan.html', context)
    else:
        form = FormAktivitas()
        context = {'form':form}
    return render(request, 'lab5/kegiatan.html', context)

def jadwal(request):
    listAktivitas =  Aktivitas.objects.all().value().order_by('tanggal')
    context = {'listAktivitas' : listAktivitas}

    return render(request,'lab5/jadwal.html',context)

def delete_all(request):
    return redirect('/jadwal/')