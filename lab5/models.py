from django.db import models

class Aktivitas(models.Model):
    nama = models.CharField(max_length=50)
    kategori = models.CharField(max_length=50)
    tempat = models.CharField(max_length=50)
    tanggal = models.DateField()
    jam = models.TimeField()

