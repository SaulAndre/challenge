from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='home'),
    path('keahlian/', views.keahlian, name='keahlian'),
    path('kontak/', views.kontak, name='kontak'),
    path('kegiatan/', views.kegiatan, name='kegiatan' ),
    path('jadwal/', views.jadwal, name='jadwal'),
    path('delete_all', views.delete_all, name='delete all'),
]