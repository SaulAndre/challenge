from django import forms
from lab5.models import Aktivitas

class FormAktivitas(forms.ModelForm):
    class Meta:
        model = Aktivitas
        fields = ['nama', 'kategori', 'tempat', 'tanggal', 'jam']
        widgets = {
            'nama' : forms.TextInput(attrs={'class':'form-control', 'placeholder':'nama kegiatan'}),
            'kategori' : forms.TextInput(attrs={'class':'form-control', 'placeholder':'kategori'}),
            'tempat' : forms.TextInput(attrs={'class':'form-control', 'placeholder':'tempat'}),
            'tanggal' : forms.DateInput(attrs={'class':'form-control', 'type': 'date'}),
            'jam' : forms.TimeInput(attrs={'class':'form-control', 'type': 'time'}),
        }