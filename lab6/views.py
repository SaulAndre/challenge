from django.shortcuts import render
from django.http import HttpResponse
from lab6.models import Status
from lab6.forms import FormStatus

# Create your views here.
def index(request):
    return render(request, "index.html")

def status(request):
    if request.method == 'POST':
        form = FormStatus(request.POST)
        if form.is_valid():
            form.save()

            form = FormStatus()
            context = {'form':form}
            return render(request, 'index.html', context)
    else:
        form = FormStatus()
        context = {'form':form}
    return render(request, 'index.html', context)


