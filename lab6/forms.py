from django import forms
from lab6.models import Status

class FormStatus(forms.ModelForm):
    class Meta:
        model = Status
        fields = ['status']
        widgets = {
            'status': forms.TextInput(attrs={'class':'form-wrapper', 'placeholder':'status'})
        }