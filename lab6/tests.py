from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, status
from .forms import FormStatus
from .models import Status
# Create your tests here.

class lab6Test(TestCase):
    def test_request_lab6(self):
        respons = Client().get('lab6/')
        self.assertEqual(respons.status_code, 200) 

    def test_template_lab6(self):
        respons = Client().get('lab6/')
        self.assertTemplateUsed(respons, 'index.html')

    def test_func_lab6(self):
        found = resolve('lab6/')
        self.assertEqual(found.func, index)

    def test_moduls_create_status(self):
        new_status = Status.object.create(status="Saya baik baik saja")
        count_status = Status.obcject.all().count()
        self.assertEqual(count_status, 1)

    def test_form_status_has_placeholder(self):
        form = FormStatus()
        self.assertIn('class="form-wrapper"', form.as_p())


    def test_form_validation_for_blank_items(self):
        form = FormStatus(data={'title': '', 'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['description'],["This field is required."])

    
